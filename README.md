![Screenshot preview of the theme "Fabricated" by glenthemes](https://64.media.tumblr.com/d680eaac1638b577190d465c65a8be56/tumblr_pablcy1UvS1ubolzro2_r1_1280.png)

**Theme no.:** 31  
**Theme name:** Fabricated  
**Theme type:** Free / Tumblr use  
**Description:** A 1-2 columned theme with a top bar and sidebar section. This preview is inspired by and features 2B from NieR: Automata.  
**Author:** @&hairsp;glenthemes  

**Release date:** 2018-06-20

**Post:** [glenthemes.tumblr.com/post/175077504729](https://glenthemes.tumblr.com/post/175077504729)  
**Preview:** [glenthpvs.tumblr.com/fabricated](https://glenthpvs.tumblr.com/fabricated)  
**Download:** [pastebin.com/ppEx62aH](https://pastebin.com/ppEx62aH)